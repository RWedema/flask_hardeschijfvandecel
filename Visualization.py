from seaborn import barplot
from io import StringIO

def draw_barplot(gff_features):
    # example dict
    # {('gene',): 1, ('TF_binding_site',): 1, ('mRNA',): 1, ('five_prime_UTR',): 1, ('CDS',): 4, ('three_prime_UTR',): 1, ('cDNA_match',): 3}
    keys, values = zip(*gff_features.items())

    cleaned_keys = []
    for (item,) in keys:
        cleaned_keys.append(item)

    print(cleaned_keys)
    print("=========")
    print(values)

    gff_barplot = barplot(x=cleaned_keys, y=list(values))
    fig = gff_barplot.get_figure()
    fig.savefig("./Static/test.png")


def draw_barplotX(gff_features):
    # example dict
    # {('gene',): 1, ('TF_binding_site',): 1, ('mRNA',): 1, ('five_prime_UTR',): 1, ('CDS',): 4, ('three_prime_UTR',): 1, ('cDNA_match',): 3}

    # come in as a string not a dictionary
    print("In the barplotX")
    print(gff_features)
    keys, values = zip(*dict(gff_features).items())

    cleaned_keys = []
    for (item,) in keys:
        cleaned_keys.append(item)

    print(cleaned_keys)
    print("=========")
    print(values)

    fig = barplot(x=cleaned_keys, y=list(values))

    img = StringIO()
    fig.savefig(img)
    img.seek(0)

    return img
