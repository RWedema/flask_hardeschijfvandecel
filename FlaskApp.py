#!/usr/bin/env python3

from flask import Flask, request, render_template, flash, redirect, url_for, send_file
from werkzeug.utils import secure_filename
import os
import DataModel
import Visualization

UPLOAD_FOLDER = 'UPLOAD_FOLDER'

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = set(['bed', 'fastq', 'fasta', 'gff', 'sam', 'vcf', 'gff3'])


@app.route('/')
def index():
    return render_template("baseTemplate.html", title="Home")


@app.route('/upload')
def upload_file():
    return render_template('formTemplate.html', title="Upload Page")


@app.route('/uploader', methods=['GET', 'POST'])
def uploader():
    if request.method == 'POST':
        f = request.files['file']

        if allowed_file(f.filename):

            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            flash('file uploaded successfully')
            in_file = (os.path.join(app.config['UPLOAD_FOLDER'], filename))
            available_features = DataModel.get_gff_features(in_file)
            Visualization.draw_barplot(available_features)

            return render_template('OutputPageTemplate.html', features=available_features, title="Outputpage")

        elif not allowed_file(f.filename):
            flash("Not an supported file extension")
            return redirect(url_for('upload_file'))


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/fig/<features>')
def fig(features):
    print("in the gif function")
    print(features)
    img = Visualization.draw_barplotX(features)

    return send_file(img, mimetype='image/png')


if __name__ == '__main__':
    app.run(debug=True)
