from BCBio import GFF
from BCBio.GFF import GFFExaminer
import pprint


def get_gff_features(filename):
    gff_file = filename
    examiner = GFFExaminer()
    in_handle = open(gff_file)

    available_features = examiner.available_limits(in_handle)
    pprint.pprint(available_features)

    in_handle.close()

    return available_features

